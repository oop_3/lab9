package com.kaewmanee.robot;

public class Unit {
    private int x;
    private int y;
    private char symbol ;
    private Map map;
    private boolean dominate;

    public Unit(Map map,char symbol,int x,int y,boolean dominate) {
        this.map = map;
        this.symbol = symbol;
        this.dominate = dominate;
        this.x = x;
        this.y = y;
    }
    public boolean isOn(int x,int y) {
        return this.x==x && this.y==y;
    }

    public boolean setXY(int x,int y) {
        if(!map.isOn(x,y)) return false;
        if(map.hasDominate(x,y)) return false;
        this.y=y;
        this.x=x;
        return true;
    }

    // 1. อยู่บน map มั้ย
    // 2. ไปทับตัวที่ dominate อื่นๆมั้ย
    public boolean setX(int x){
        if(!map.isInWidth(x)) return false;
        if(map.hasDominate(x,y)) return false;
        this.x=x;
        return true;
    }

    public boolean setY(int y){
        if(!map.isInHeight(y)) return false;
        if(map.hasDominate(x,y)) return false;
        this.y=y;
        return true;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public boolean isDominate() {
        return dominate;
    }
    public Map getMap() {
        return map;
    }
    public char getSymbol() {
        return symbol;
    }
    public String toString() {
        return "Uint ("+this.symbol+") ["+this.x +" , "+this.y +"] is on "+map;
    }
    
}
